<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Spending;
use App\Entity\SpendingType;
use App\Entity\Participant;
use App\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SpendingController extends AbstractController
{
    /**
     * @Route("/spend", name="spend", methods="GET|POST")
     */
    public function spend(Request $request): Response
    {
        
        //Instancier la campaign
        $campaign_id =  $request->request->get('campaign_id');
        $campaign = $this->getDoctrine()
                        ->getRepository(Campaign::class)
                        ->find($campaign_id);

        //Enregistrer un participant
        $participant = new Participant();

        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));


        $participant->setCampaign($campaign);
        
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();


        //Enregistrer le payment qui est dépendant du participant

        $payment = new Payment();

        $payment->setAmount($request->request->get("amount") * 100);

        $payment->setParticipant($participant);

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        //Redirection vers la fiche campagne
        return $this->redirectToRoute('campaign_show', [
            "id" =>  $campaign_id
        ]);


    }
}
