<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Entity\Participant;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        
        $campaign = new Campaign();

        $id = md5(random_bytes(50));
        $campaign->setId($id);
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        

        $campaign->setAuthor($request->request->get('author'));
        

        
        
         //dd($campaign);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($campaign);
            $em->flush();
           

            return $this->redirectToRoute('campaign_show', [
                "id"=> $id
            ]);
        }

        return $this->render('campaign/new.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
            //'campaign_name' => $campaign_name,
        ]);
        
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET") 
     */
    public function show(Campaign $campaign): Response
    {
        //Récupération des participants et du montant qu'ils ont donné
        $em = $this->getDoctrine()->getManager();

        $query= 'SELECT * FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        wHERE campaign_id="'. $campaign->getId() .'"';

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();
        $results = $statement->fetchAll();

        $total_amount = 0;
        $total_participant = count($results);

        for($i = 0; $i <= ($total_participant - 1); $i++)
        {
            $total_amount += $results[$i]["amount"];
        }

        $total_amount2 = $total_amount / 100;
        $objectif = round($total_amount2 / $campaign->getGoal() * 100);
        

        
        return $this->render('campaign/show.html.twig', compact('campaign', 'results', 'total_participant', "total_amount2", "objectif"));
    }

    /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Campaign $campaign): Response
    {
        $amount_participant = $_GET['amount_participant'] ?? "";
        return $this->render('campaign/pay.html.twig', compact('campaign', 'amount_participant'));
    }

    /**
     * @Route("/{id}/spend", name="campaign_spend", methods="GET|POST")
     */
    public function spend(Campaign $campaign): Response
    {
        $amount_participant = $_GET['amount_participant'];
        return $this->render('campaign/spend.html.twig', compact('campaign', 'amount_participant'));
    }



    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
}
